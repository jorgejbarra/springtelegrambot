package com.jorgejbarra.springtelegrambot;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Component
@Path("/test")
@Produces("application/json")
public class TestController {


    @GET

    public ResponseEntity<String> getAllBooks() {
        return ResponseEntity.ok("Ole yo");
    }
}
